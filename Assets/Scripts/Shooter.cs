﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{

    public GameObject projectile, gun;
    public string PROJECTILES = "Projectiles";

    private GameObject projectileParent;
    private Animator _animator;
    private Spawner _spawner;

    void Start()
    {
        _animator = GameObject.FindObjectOfType<Animator>();

        projectileParent = GameObject.Find(PROJECTILES);
        if (!projectileParent)
        {
            projectileParent = new GameObject(PROJECTILES);
        }

        findSpawner();
    }


    void Update()
    {
        if (IsEnemyAhead())
        {
            _animator.SetBool("isAttacking", true);
        }
        else
        {
            _animator.SetBool("isAttacking", false);
        }
    }

    private bool IsEnemyAhead()
    {
        foreach (Transform attacker in _spawner.transform)
        {
            float attackerPosition = attacker.transform.position.x;
            if (attackerPosition > transform.position.x && attackerPosition < 10)
            {
                return true;
            }
        }
        return false;
    }

    private void Fire()
    {
        GameObject newProjectile = Instantiate(projectile);
        newProjectile.transform.parent = projectileParent.transform;
        newProjectile.transform.position = gun.transform.position;

    }

    private void findSpawner()
    {
        Spawner[] spawners = FindObjectsOfType<Spawner>();

        foreach (Spawner spawnPoint in spawners)
        {
            if (spawnPoint.transform.position.y == transform.position.y)
            {
                _spawner = spawnPoint;
                return;
            }
        }

        if (!_spawner)
        {
            Debug.Log("spawner not found");
        }
    }
}
