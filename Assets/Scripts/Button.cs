﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button : MonoBehaviour
{

    public Defender defenderPrefab;
    public static Defender selectedDefender;

    private Text costText;
    private Button[] buttonArray;


    // Use this for initialization
    void Start()
    {
        buttonArray = GameObject.FindObjectsOfType<Button>();
        costText = GetComponentInChildren<Text>();
        costText.text = defenderPrefab.starCost.ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnMouseDown()
    {
        selectedDefender = defenderPrefab;
        foreach (Button button in buttonArray)
        {
            button.GetComponent<SpriteRenderer>().color = Color.black;
        }
        GetComponent<SpriteRenderer>().color = Color.white;
    }
}
