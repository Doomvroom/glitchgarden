﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour
{

    public AudioClip[] levelMusicChangeArray;
    private AudioSource audioSource;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        DontDestroyOnLoad(gameObject);

    }

    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = PlayerPrefsManager.GetMasterVolume();
    }

    void OnLevelWasLoaded(int level)
    {
        if (level < levelMusicChangeArray.Length)
        {
            AudioClip levelMusic = levelMusicChangeArray[level];
            if (levelMusic)
            {
                audioSource.clip = levelMusic;
                audioSource.loop = true;
                audioSource.Play();
            }
        }


    }

    public void SetVolume(float volume)
    {
        audioSource.volume = volume;
    }
}
