﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{

    public int EndTime = 360;
    private Slider slider;
    private AudioSource audioSource;
    private bool isEndOfLevel = false;
    private LevelManager levelManager;
    private GameObject winLabel;

    // Use this for initialization
    void Start()
    {
        slider = GetComponent<Slider>();
        audioSource = GetComponent<AudioSource>();
        levelManager = GameObject.FindObjectOfType<LevelManager>();

        winLabel = GameObject.Find("Win Condition");
        winLabel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
       float currentTime = Time.timeSinceLevelLoad;
        if (currentTime >= EndTime && !isEndOfLevel)
        {
            isEndOfLevel = true;
            WinCondition();
        }
        slider.value = currentTime / EndTime;
    }
    void WinCondition()
    {
        DestroyAllTaggedObjects();
        audioSource.Play();
        winLabel.SetActive(true);
        Invoke("LoadNextLevel", audioSource.clip.length);
    }

    private void DestroyAllTaggedObjects()
    {
        var objects = GameObject.FindGameObjectsWithTag("destroyOnWin");
        foreach(GameObject obj in objects)
        {
            Destroy(obj);
        }

    }

    void LoadNextLevel()
    {
        levelManager.LoadNextLevel();
    }
     
}
