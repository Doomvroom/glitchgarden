﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetStartVolume : MonoBehaviour
{
    private MusicManager musicManager;
    void Start()
    {
        musicManager = GameObject.FindObjectOfType<MusicManager>();
        if (musicManager)
        {
            musicManager.SetVolume(PlayerPrefsManager.GetMasterVolume());
        }
        else
        {
            Debug.LogWarning("No music manager found in Start scene, can't set volume");
        }
    }

}
