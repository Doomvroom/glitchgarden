﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class StarDisplay : MonoBehaviour
{

    private Text _starDisplay;
    private int starCount = 100;
    public enum Status { SUCCESS, FAIL };

    void Start()
    {
        _starDisplay = GetComponent<Text>();
        UpdateDisplay();
    }

    public void AddStars(int amount)
    {
        starCount += amount;
        UpdateDisplay();
    }

    private void UpdateDisplay()
    {
        _starDisplay.text = starCount.ToString();
    }

    public Status UseStars(int amount)
    {
        if (starCount >= amount)
        {
            starCount -= amount;
            UpdateDisplay();
            return Status.SUCCESS;
        }

        return Status.FAIL;
    }
}
