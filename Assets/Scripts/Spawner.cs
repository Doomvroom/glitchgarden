﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject[] attackerPrefabArray;

    private Dictionary<string, float> spawnTimes = new Dictionary<string, float>();

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject thisAttacker in attackerPrefabArray)
        {
            if (isTimeToSpawn(thisAttacker))
            {
                Spawn(thisAttacker);
            }
        }
    }

    private bool isTimeToSpawn(GameObject attacker)
    {
        string key = attacker.name;

        if (spawnTimes.ContainsKey(key))
        {
            float timeLeft = spawnTimes[key];
            timeLeft -= Time.deltaTime;
            spawnTimes[key] = timeLeft;
            if (spawnTimes[key] <= 0)
            {
                var meanSpawnDelay = attacker.GetComponent<Attacker>().seenEverySeconds;
                var spawnsPerSecond = 1 / meanSpawnDelay;
                float threshhold = spawnsPerSecond * Time.deltaTime / 5;

                if(UnityEngine.Random.value < threshhold)
                {
                    spawnTimes.Remove(key);
                    return true;
                }
            }
        }
        else
        {
            var timeToSpawn = attacker.GetComponent<Attacker>().seenEverySeconds;
            spawnTimes.Add(key, timeToSpawn);
        }
        return false;
    }

    private void Spawn(GameObject myGameObject)
    {
        GameObject attacker = Instantiate(myGameObject);
        attacker.transform.parent = gameObject.transform;
        attacker.transform.position = gameObject.transform.position;
    }
}
