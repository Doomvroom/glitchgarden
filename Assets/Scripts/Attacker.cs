﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class Attacker : MonoBehaviour
{
    [Tooltip("Average number of seconds between appearances")]
    public float seenEverySeconds;
    private float currentSpeed;
    private GameObject currentTarget;
    private Animator anim;

    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.left * currentSpeed * Time.deltaTime);

        if (!currentTarget)
            anim.SetBool("isAttacking", false);
    }

    void OnTriggerEnter2D()
    {
        Debug.Log(name + "trigger enter");
    }

    public void SetSpeed(float speed)
    {
        currentSpeed = speed;
    }

    //called from the animator at the time of an actual attack
    public void StrikeCurrentTarget(float damage)
    {
        Debug.Log(name + "caused damage: " + damage.ToString());

        if (currentTarget)
        {
            Health targetHealth = currentTarget.GetComponent<Health>();
            if (targetHealth)
                targetHealth.RemoveHealth(damage);
        }

    }

    public void Attack(GameObject defender)
    {
        currentTarget = defender;
    }

}
