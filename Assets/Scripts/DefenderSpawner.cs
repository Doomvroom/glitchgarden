﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderSpawner : MonoBehaviour
{
    private string PARENTOBJECT = "Defenders";
    private GameObject parent;
    private Camera myCamera;
    private StarDisplay _starDisplay;

    void Start()
    {
        myCamera = GameObject.FindObjectOfType<Camera>();
        parent = GameObject.Find(PARENTOBJECT);
        if (!parent)
        {
            parent = new GameObject(PARENTOBJECT);
        }
        _starDisplay = GameObject.FindObjectOfType<StarDisplay>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseDown()
    {
        if (Button.selectedDefender  && _starDisplay.UseStars(Button.selectedDefender.starCost) == StarDisplay.Status.SUCCESS)
        {
            var currentPosition = Input.mousePosition;
            Vector2 worldPosition = CalculateWorldPointOfMouseClick(currentPosition.x, currentPosition.y);
            var defender = Instantiate(Button.selectedDefender, worldPosition, Quaternion.identity);
            defender.transform.parent = parent.transform;
        }
    }

    Vector2 CalculateWorldPointOfMouseClick(float x, float y)
    {
        float distanceFromCamera = 10f;
        Vector3 weirdTriplet = new Vector3(x, y, distanceFromCamera);
        Vector2 worldPosition = myCamera.ScreenToWorldPoint(weirdTriplet);
        return SnapToPosition(worldPosition);
    }

    Vector2 SnapToPosition(Vector2 position)
    {
        float x = Mathf.RoundToInt(position.x);
        float y = Mathf.RoundToInt(position.y);
        return new Vector2(x, y);
    }
}
